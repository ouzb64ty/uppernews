const path = require('path');

const config = {
    entry: './app.jsx',
    output: {
        path: path.resolve(__dirname, 'public/dist/'),
        filename: 'app.js',
        publicPath: 'public/dist/'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)/,
                use: 'babel-loader',
                exclude: /node_modules/
            }
        ]
    }
}

module.exports = config;
