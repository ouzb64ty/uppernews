/* Requires */

const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const mysql = require('mysql');
const argon2 = require('argon2');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUiExpress = require('swagger-ui-express');
const { uuid } = require('uuidv4');
const redis = require('redis');

/* Config */

const port = process.env.PORT || 8001;
var connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: 'root',
    database: 'uppernews'
});
const redisClient = redis.createClient();
const swaggerDocs = swaggerJsDoc({
    swaggerDefinition: {
        info: {
            title: 'Uppernews API',
            description: 'The new generation syndication world.',
            contact: {
                name: 'ouzb64ty'
            },
            servers: ['http://localhost:8000']
        }
    },
    apis: ['index.js']
});
/* Move in ENV */
const secretKey = "UNnWQ%?4uG%X#*ktP2e32xmuKa+#_s*BMGwbZ?bA#G&Fuu8rLxP7dBydZhXh^F!k?9c-jq?=m2JfUrEwezy3LRhnDM%AfWpBrj6FLTrtNjg=^-2$VHrK+r^e_bSg^nfvJ3nWrFnK9XMVCMn=%r?B%2p7EAA8YC6mCw#ZxBAjZ!JQsY&vyF+3aXUQCeVFV2@6mm#6h22gVHQ!8q#dHFN%%py^YTZND&-65CzFPf*!!4MuDCnJK5x8@pSDCdk%t$qK";

connection.connect();
app.use('/apiDoc', swaggerUiExpress.serve, swaggerUiExpress.setup(swaggerDocs));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/* Bearer Token */

function managePrivileges(req, res, next) {

    const bearerHeader = req.headers['authorization'];

    if (typeof bearerHeader != 'undefined') {
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];
        req.token = bearerToken;
    }
    next();
}

/* Models */

let query = (connection, request) => {
    return new Promise((resolve, reject) => {
        connection.query(request, (error, results, fields) => {
            if (error) throw error;

            resolve(results);
        });
    });
}

let generateToken = (user) => {
    let payload = {
        id: user.id,
        username: user.username,
        date: user.date
    };
    let signature = {
        algorithm: 'HS512',
        expiresIn: 60 * 15
    };
    let token = jwt.sign(payload, secretKey, signature);
    return token;
};

/* Routes */

/**
 * @swagger
 *  /news:
 *      get:
 *          description: Use to get all news
 *          responses:
 *              '200':
 *                  description: Send result with all news
 *              '500':
 *                  description: Internal error
 */

app.get('/news', managePrivileges, async(req, res) => {
    console.log('ok');
    try {
        const request = 'SELECT * FROM news';
        const news = await query(connection, request);

        if (news.length > 0) {
            res.statusCode = 200;
            res.json({ 'result': news });
        } else {
            res.statusCode = 200;
            res.json({ 'result': 'News not found' });
        }
    } catch (e) {
        res.statusCode = 500;
        res.json({ 'result': 'Internal server error' });
    }
});

/**
 * @swagger
 *  /news:
 *      post:
 *          description: Use to publish a news
 *          responses:
 *              '201':
 *                  description: News correctly published
 *              '401':
 *                  description: Access token expired or wrong
 *              '500':
 *                  description: Internal error
 */

app.post('/news', managePrivileges, (req, res) => {
    const title = req.body.title;
    const description = req.body.description || '';
    const url = req.body.url;

    if (title
        && description) {
        jwt.verify(req.token, secretKey, async(err, decoded) => {
            if (err) {
                res.statusCode = 401;
                res.json({
                    'result': "Access Token expired or wrong",
                    'advice': 'Refresh your access token'
                });
            } else {
                try {
                    const titleEsc = mysql.escape(req.body.title);
                    const descriptionEsc = mysql.escape(req.body.description);
                    const urlEsc = mysql.escape(req.body.url);
                    const request = 'INSERT INTO news VALUES (NULL, ' + titleEsc + ', ' + descriptionEsc + ', NOW(), ' + urlEsc + ', ' + decoded.id + ')';
                    const insertNewsRes = await query(connection, request);

                    res.statusCode = 201;
                    res.json( { result : 'News correctly published' });
                } catch(err) {
                    res.statusCode = 500;
                    res.json( { result : 'Internal error' });
                }
            }
        });
    } else {
        res.statusCode = 400;
        res.json( {
            result : 'Incorrect publication news format',
            advice : 'You must specify title and description.'
        });
    }
});

/**
 * @swagger
 *  /refresh:
 *      post:
 *          description: Use to refresh token
 *          responses:
 *              '200':
 *                  description: Access token successfully refreshed, send new access token
 *              '400':
 *                  description: Unknown refresh token for this username or refresh token not equal
 */

app.post('/refresh', managePrivileges, (req, res) => {
    const user = jwt.decode(req.token);
    const refreshToken = req.body.refreshToken;

    redisClient.hget(user.username, 'refreshToken', (err, result) => {
        if (err) {
            res.statusCode = 400;
            res.json( { result : 'Unknown refresh token for this username' });
        } else {
            if (refreshToken === result) {
                const accessToken = generateToken(user);

                res.statusCode = 200;
                res.json({
                    'accessToken': accessToken,
                    'result': 'Access token successfully refreshed'
                });
            } else {
                res.statusCode = 400;
                res.json({
                    'result': 'Refresh token not equal',
                    'advice': 'Try to login you'
                });
            }
        }
    });
});

/**
 * @swagger
 *  /register:
 *      post:
 *          description: Use to login an existing user with JWT
 *          responses:
 *              '200':
 *                  description: Successfull login, send access and refresh token
 *              '400':
 *                  description: Password must be greater than 8 characters unknown username or wrong password
 *              '500':
 *                  description: Internal error
 */

app.post('/login', async(req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    try {
        if (username
            && password
            && password.length >= 8) {
            const usernameEsc = mysql.escape(username);
            const request = "SELECT * FROM users WHERE username=" + usernameEsc;
            const user = await query(connection, request);

            if (user.length !== 0) {
                if (await argon2.verify(user[0].password, password)) {
                    let accessToken = generateToken(user[0]);
                    let refreshToken = uuid();

                    redisClient.hmset(username, ['refreshToken', refreshToken], (err, redisRes) => {
                        res.statusCode = 200;
                        res.json({
                            'accessToken': accessToken,
                            'refreshToken': refreshToken,
                            'result': 'Tokens successfully send'
                        });
                    });
                } else {
                    res.statusCode = 400;
                    res.json( { result : 'Wrong password' });
                }
            } else {
                res.statusCode = 400;
                res.json( { result : 'Unknown username' });
            }
        } else {
            res.statusCode = 400;
            res.json( {
                result : 'Wrong authentication',
                advice : 'Password must be greater than 8 characters.'
            });
        }
    } catch (err) {
        res.statusCode = 500;
        res.json( { result : 'Internal error' });
    }
});

/**
 * @swagger
 *  /register:
 *      post:
 *          description: Use to register a new user
 *          responses:
 *              '201':
 *                  description: Successfull register
 *              '400':
 *                  description: Username already exist or Wrong authentication
 *              '500':
 *                  description: Internal error
 */

app.post('/register', async(req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    const confPassword = req.body.confPassword;

    try {
        if (username
            && password
            && confPassword
            && password.length >= 8
            && confPassword === password) {
            let escUsername = connection.escape(username);
            let request = 'SELECT id FROM users WHERE username=' + escUsername;
            let userExist = await query(connection, request);

            if (userExist.length == 0) {
                let hashPassword = await argon2.hash(password);
                let escHashPassword = connection.escape(hashPassword);
                let request = 'INSERT INTO users VALUES (NULL, ' + escUsername + ', ' + escHashPassword + ', NOW())';
                let registerRes = await query(connection, request);

                res.statusCode = 201;
                res.json( { result : 'Successfull register', advice : 'You can now log in' });
            } else {
                res.statusCode = 400;
                res.json( { result : 'Username already exist' });
            }
        } else {
            res.statusCode = 400;
            res.json( {
                result : 'Wrong authentication',
                advice : 'Password must be greater than 8 characters and password must be equals to confirmation password.'
            });
        }
    } catch (err) {
        res.statusCode = 500;
        res.json( { result : 'Internal error' });
    }
});

/* Launch API */

app.listen(port);
