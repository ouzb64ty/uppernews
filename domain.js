/* Requires */

const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const axios = require('axios');

/* Config */

const port = process.env.PORT || 8000;
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

/* Bearer Token */

function managePrivileges(req, res, next) {

    const bearerHeader = req.headers['authorization'];

    if (typeof bearerHeader != 'undefined') {
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];
        req.token = bearerToken;
    }
    next();
}

/* Routes */

app.get('/', (req, res) => {
    res.render('index');
});

app.get('/json', async (req, res) => {
    try {
        const news = await axios.get('http://127.0.0.1:8001/news');

        res.json(news.data);
    } catch (err) {
        if (err) throw err;
    }
});

/* Launch API */

app.listen(port);
